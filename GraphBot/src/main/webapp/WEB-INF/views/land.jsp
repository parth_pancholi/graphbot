<!DOCTYPE html>
<html>
<head>
<title>GraphBot</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script language="javascript" type="text/javascript" src="resources/js/jquery-1.9.1.min.js"></script>
<script language="javascript" type="text/javascript" src="resources/js/graphbot.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<style type="text/css">
* { margin:0; padding:0}
*, *:before, *:after {  -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;}
.cf:before, .cf:after{content:" ";display:table}
.cf:after{clear:both}
body{color:#222;font-family: 'Open Sans', sans-serif;font-size:14px;line-height:1.5; overflow:hidden; max-width:410px; margin:0 auto;}
#content{width:100%;}
#action-space{display:inline;float:left;padding-left:20px;}
#history{width:100%;height:calc(100vh - 50px);background-color:#fff;overflow-y:scroll;padding:10px 0 0;}
.human-speech, .bot-speech{clear:both;padding:0;}
.human-speech > .inner, .bot-speech > .inner{padding:5px 10px;max-width:calc(90% - 20px);text-align:left;margin:7px 10px; position:relative}
.human-speech > .inner{float:right;background-color:#c7e3ed;color:#000;-moz-border-radius-topleft: 10px;-webkit-border-top-left-radius: 10px; border-top-left-radius: 10px;-moz-border-radius-topright: 10px;-webkit-border-top-right-radius: 10px;border-top-right-radius: 10px;-moz-border-radius-bottomleft: 10px;-webkit-border-bottom-left-radius: 10px;border-bottom-left-radius: 10px;}
.human-speech > .inner:before, .bot-speech > .inner:before { content:""; width:8px; height:9px; background-repeat:no-repeat; position:absolute; bottom:0}
.human-speech > .inner:before {background-position:left bottom; left:100%; background-image:url(resources/images/human-reply-bg.png)}
.bot-speech > .inner{float:left;background-color:#ecc7ed;color:black;-moz-border-radius-topleft: 10px;-webkit-border-top-left-radius: 10px; border-top-left-radius: 10px;-moz-border-radius-topright: 10px;-webkit-border-top-right-radius: 10px;border-top-right-radius: 10px;-moz-border-radius-bottomright: 10px;-webkit-border-bottom-right-radius: 10px;border-bottom-right-radius: 10px;}
.bot-speech > .inner:before {background-position:right bottom;right:100%; background-image:url(resources/images/bot-reply-bg.png)}
.btn { display:block; text-align:center; padding:5px 0;}
.btn a { display:inline-block; background-color:#0096ff; border:2px solid #0096ff; color:#fff; font-size:16px; font-weight:bold;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius:5px; padding:5px 20px; text-decoration:none;}
.btn a:hover { background-color:#fff; color:#0096ff;}
#user-says{width:100%;background-color:#d2d2d2; color:#000; border:none; font-size:16px; padding:10px 60px 10px 10px; height:50px}
#entry {position:relative;width:100%;padding:0;background-color:#fff;}
#send-button {display:block; width:35px; height:35px; background:url(resources/images/btn-submit.png) no-repeat center center; position:absolute; right:15px; top:50%; margin-top:-18px;overflow: hidden;text-indent: -1000px;}
</style>
</head>
<body>
<div id="content">
  <div id="history"></div>
  <div id="entry">
    <input type="text" id="user-says" placeholder="Enter your message"/><a id="send-button" href="javascript:void(0)" >send</a>
  </div>
</div>
<div id="action-space"></div>
</body>
</html>
