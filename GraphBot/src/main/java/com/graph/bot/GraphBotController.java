
package com.graph.bot;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.ibm.watson.developer_cloud.conversation.v1.ConversationService;
import com.ibm.watson.developer_cloud.conversation.v1.model.Intent;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageRequest;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;

/**
 * Simple web page controller for chat-bot interface.
 */
@Controller
//@RequestMapping("/controller")
public class GraphBotController {

	private static final Logger logger = LoggerFactory.getLogger(GraphBotController.class);

	/**
	 * Simple wrapper for a chat-bot response.
	 */
	class ChatBotResponse {
		String text = "Welcome";
		String action = null;

		public ChatBotResponse() {
			super();
		}

		/**
		 * Response message from chat-bot
		 */
		public String getText() {
			return (text);
		}

		/**
		 * Optional action associated with this response, as defined by ??
		 */
		public String getAction() {
			return (action);
		}
	}

	// Instance variables

	@Autowired
	private ServletContext context;

	@Autowired
	private ApplicationContext applicationContext;

	private String workspaceID = null;
	private String username = null;
	private String password = null;
	private ConversationService service = null;

	/**
	 *
	 */
	@PostConstruct
	public void init() {
		service = new ConversationService(ConversationService.VERSION_DATE_2016_07_11);
		service.setUsernameAndPassword(username, password);
	}

	/**
	 * Simple setter for bean initialisation.
	 */
	public void setWorkspace(String workspaceID) {
		this.workspaceID = workspaceID;
	}

	/**
	 * Simple setter for bean initialisation.
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Simple setter for bean initialisation.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Basic root page mapping to land.html
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String landingPage(HttpSession session) {
		logger.info("landingPage called..");
		return "land";
	}

	/**
	 * Mapping for user message request/response, with chat-bot interface.
	 * jackson-mapper-asl dependency for producing JSON response in Spring.
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST, params = {
			"action=usersays" }, produces = "application/json")
	public @ResponseBody String userSays(HttpSession session, @RequestParam("text") String usermsg) {
		// Instantiate a new message builder
		MessageRequest.Builder builder = new MessageRequest.Builder();

		// If we have a context in our session, add it to the message builder
		Map<String, Object> msgContext = (Map<String, Object>) session.getAttribute("conversation-context");
		if (msgContext != null)
			builder.context(msgContext);

		// Add the user message to the builder
		builder.inputText(usermsg);

		// Build and send the message to the chat-bot workspace
		MessageRequest msgReq = builder.build();
		MessageResponse msgResp = service.message(workspaceID, msgReq).execute();

		// Add the response context to the session (to maintain state)
		msgContext = msgResp.getContext();
		session.setAttribute("conversation-context", msgContext);

		// Encapsulate the response in a simple wrapper
		ChatBotResponse botResp = new ChatBotResponse();

		// Check the response's intents
		if (((List) msgResp.getIntents()).size() > 0) {
			Intent intent = msgResp.getIntents().get(0);
			System.out.println("intent: #" + intent.getIntent());

			// Special case - if the intent is "goodbye", then clear this
			// session ... how to make generic?
			if (intent.getIntent().equals("goodbye"))
				session.removeAttribute("conversation-context");
		}

		// Does the response feature an "action" node?
		String action = (String) msgResp.getOutput().get("action");
		if (action != null) {
			System.out.println("ACTION: " + msgResp.getOutput().get("action"));
			botResp.action = action;
		}

		// Pick up the chat-bot respnse message
		if (((List) msgResp.getOutput().get("text")).size() > 0) {
			botResp.text = ((List<String>) msgResp.getOutput().get("text")).get(0);
			System.out.println(botResp.text);
		}

		Gson gson = new Gson();
		return gson.toJson(botResp);
	}
}
